package com.danghai.ps10201lab04;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class Bai03Activity extends AppCompatActivity {

    final String IMAGE_URL = "http://heobaka.info/bg.png";
    final String DIR = "sdcard/photoalbum";
    final String IMAGE_NAME = "downloaded_image.jpg";
    final int MY_PERMISSIONS_REQUEST_INTERNET = 923;
    final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 924;

    EditText imageLink;
    Button btnDownload;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bai03);
        requestPermissionWriteExternalStorage();
        requestPermissionInternet();

        btnDownload = findViewById(R.id.btn_download);
        imageView = findViewById(R.id.image_view);
        imageLink = findViewById(R.id.image_link);



        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (haveNetwork()){

                    String imageUrl = imageLink.getText().toString().trim();
                    if (!isValImageUrl(imageUrl)){
                        Toast.makeText(Bai03Activity.this, "Vui lòng kiểm tra lại link ảnh", Toast.LENGTH_SHORT).show();
                    }else{
                        //Call method
                        DownloadTask downloadTask = new DownloadTask();
                        downloadTask.execute(imageUrl);
                    }
                }else{
                    Toast.makeText(Bai03Activity.this, "Bạn chưa có kết nối internet!", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private boolean isValImageUrl(String str){
        String regex = "^(http(s?):)([/|.|\\w|\\s|-])*\\.(?:jpg|gif|png)$";

        if (!str.matches(regex)){
            return false;
        }
        return true;
    }

    private void requestPermissionInternet() {
        if (ContextCompat.checkSelfPermission(Bai03Activity.this,
                Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(Bai03Activity.this,
                    Manifest.permission.INTERNET)) {

            } else {

                ActivityCompat.requestPermissions(Bai03Activity.this,
                        new String[]{Manifest.permission.INTERNET},
                        MY_PERMISSIONS_REQUEST_INTERNET);
            }
        } else {

        }
    }

    private void requestPermissionWriteExternalStorage() {
        if (ContextCompat.checkSelfPermission(Bai03Activity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(Bai03Activity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            } else {

                ActivityCompat.requestPermissions(Bai03Activity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);


            }
        } else {

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_INTERNET:
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                }
                return;
            }

        }
    }

    private boolean haveNetwork(){
        boolean have_WIFI = false;
        boolean have_MobileData = false;

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);


        NetworkInfo[] networkInfos = connectivityManager.getAllNetworkInfo();

        for (NetworkInfo info: networkInfos){
            if (info.getTypeName().equalsIgnoreCase("WIFI")){
                if (info.isConnected()) {
                    have_WIFI = true;
                }
            }
            if (info.getTypeName().equalsIgnoreCase("MOBILE")){
                if (info.isConnected()) {
                    have_MobileData = true;
                }

            }
        }
        return have_WIFI || have_MobileData;
    }


    class DownloadTask extends AsyncTask<String, Integer, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //Thiet lap ProgressDialog
            progressDialog = new ProgressDialog(Bai03Activity.this);
            progressDialog.setTitle("Download in Progress...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setMax(100);
            progressDialog.setProgress(0);
            progressDialog.show();


        }

        @Override
        protected String doInBackground(String... voids) {
            //Xu ly tien trinh download trên ProgressDialog khi duoc Click vao button
            String path = voids[0];
            int fileLenght = 0;
            try {
                //Lay url
                URL url = new URL(path);
                URLConnection urlConnection = url.openConnection();
                urlConnection.connect();
                //Lay dung luong cua file
                fileLenght = urlConnection.getContentLength();

                //Duong dan luu file
                File newFolder = new File(DIR);
                if (!newFolder.exists()) {
                    newFolder.mkdirs();

                }
                //Dat lai ten file anh
                File inputFile = new File(newFolder, IMAGE_NAME);

                //Bat dau ghi file truyen vao bang BufferedInputStream
                InputStream inputStream = new BufferedInputStream(url.openStream(), 8192); //Default size is 8kb = 8192 byte
                byte[] data = new byte[1024]; //1kb
                int total = 0;
                int count = 0;
                OutputStream outputStream = new FileOutputStream(inputFile);
                //Hien thi so % duoc download
                while ((count = inputStream.read(data)) != -1) {
                    total += count;
                    outputStream.write(data, 0, count);
                    int progress = (int) total * 100 / fileLenght;

                    publishProgress(progress);

                }
                //Dong tien trinh
                inputStream.close();
                outputStream.close();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            return "Download complete...";
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            //Gan gia tri % tren ProgressDialog
            progressDialog.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.hide();
            Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
            //Duong dan den file anh de show ra imageView
            String path = DIR + "/" + IMAGE_NAME;
            //Gan anh len imageView
            imageView.setImageDrawable(Drawable.createFromPath(path));
        }


        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    }
}
