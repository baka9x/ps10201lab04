package com.danghai.ps10201lab04;

import androidx.appcompat.app.AppCompatActivity;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.Toast;

public class Bai02Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bai02);

        if (haveNetwork()){
            Toast.makeText(this, "Bạn đã kết nối internet!", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "Bạn chưa có kết nối internet!", Toast.LENGTH_SHORT).show();
        }


    }

    private boolean haveNetwork(){
        boolean have_WIFI = false;
        boolean have_MobileData = false;

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);


        NetworkInfo[] networkInfos = connectivityManager.getAllNetworkInfo();

        for (NetworkInfo info: networkInfos){
            if (info.getTypeName().equalsIgnoreCase("WIFI")){
                if (info.isConnected()) {
                    have_WIFI = true;
                }
            }
            if (info.getTypeName().equalsIgnoreCase("MOBILE")){
                if (info.isConnected()) {
                    have_MobileData = true;
                }

            }
        }
        return have_WIFI || have_MobileData;
    }
}
